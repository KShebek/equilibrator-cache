equilibrator-cache
==================

[![pipeline status](https://gitlab.com/elad.noor/equilibrator-cache/badges/develop/pipeline.svg)](https://gitlab.com/elad.noor/equilibrator-cache/commits/develop)

[![coverage report](https://gitlab.com/elad.noor/equilibrator-cache/badges/develop/coverage.svg)](https://gitlab.com/elad.noor/equilibrator-cache/commits/develop)

[![Join the chat at https://gitter.im/equilibrator-devs/equilibrator-api](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/equilibrator-devs/equilibrator-api?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

A database application for caching data used by eQuilibrator and related projects.
Stored data includes compound names, structures, protonation state information,
reaction and enzyme info, and cross-references to other databases.
All compounds stored in equilibrator-cache are cross-referenced using
[InChIKey](https://en.wikipedia.org/wiki/International_Chemical_Identifier#InChIKey).

## Supported Chemical Databases:

* [MetaNetX](https://www.metanetx.org/)
* [KEGG](https://www.kegg.jp/)
* [ChEBI](https://www.ebi.ac.uk/chebi/)

